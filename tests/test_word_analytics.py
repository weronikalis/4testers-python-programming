from src.word_analytics import filter_words_containing_letter_a


def test_filtering_words_for_empty_list():
    filtered_list = filter_words_containing_letter_a([])
    assert filtered_list == []


def test_filtering_words_for_integers_list():
    filtered_list = filter_words_containing_letter_a([1, 2, 3])
    assert filtered_list == []


def test_filtering_words_for_special_characters_list():
    filtered_list = filter_words_containing_letter_a(["@", "#"])
    assert filtered_list == []


def test_filtering_words_for_list_not_containing_any_word_with_letter_a():
    filtered_list = filter_words_containing_letter_a(["pies", "smok", "kot"])
    assert filtered_list == []


def test_filtering_words_for_list_not_containing_any_word_with_letter_a():
    filtered_list = filter_words_containing_letter_a(["pies", "smok", "kot"])
    assert filtered_list == []


def test_filtering_words_for_list_containing_mixed_single_letters():
    filtered_list = filter_words_containing_letter_a(["a", "b", "a"])
    assert filtered_list == ["a", "a"]


def test_filtering_words_for_list_containing_mixed_letters_and_integers():
    filtered_list = filter_words_containing_letter_a([1, "a", 2, "a"])
    assert filtered_list == ["a", "a"]


def test_filtering_words_for_list_containing_only_words_with_letter_a():
    filtered_list = filter_words_containing_letter_a(["paka", "maka", "buka"])
    assert filtered_list == ["paka", "maka", "buka"]


def test_filtering_words_for_list_containing_words_with_and_without_letter_a():
    filtered_list = filter_words_containing_letter_a(["kot", "żaba"])
    assert filtered_list == ["żaba"]
