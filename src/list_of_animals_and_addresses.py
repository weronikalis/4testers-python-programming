# Zadanie 2
animals = [
    {"name": "Chrupek", "kind": "dog", "age": 3},
    {"name": "Pankracy", "kind": "cat", "age": None},
    {"name": "Atos", "kind": "hamster", "age": 1},
]

print(animals[-1]["name"])
animals[1]["age"] = 2
print(animals[1])
animals.insert(0, {"name": "Reksio", "kind": "dog", "age": 5})
print(animals)

# Zadanie 2

addresses = [
    {"city": "Wroclaw", "street": "Swidnicka", "house_number": 20, "post_code": "51-223"},
    {"city": "New York", "street": "Avenue J", "house_number": 1424, "post_code": 11230},
    {"city": "London", "street": "Tothill St", "house_number": 21, "post_code": "SW1H 9LL"}
]

print(addresses[2]["post_code"])
print(addresses[1]["city"])
addresses[0]["street"] = "Olawska"
print(addresses)
