def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło mi Cię widzieć w naszym mieście: {city}!")

def generate_email_for_4testers(email_name, email_surname):
    email_name = email_name.lower()
    email_surname = email_surname.lower()
    print(f"{email_name}.{email_surname}@4testers.pl")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Kasia", "Szczecin")
    print_greetings_for_a_person_in_the_city("Basia", "Gdynia")

    generate_email_for_4testers("Janusz", "Nowak")
    generate_email_for_4testers("Barbara", "Kowalska")


