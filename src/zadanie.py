import random
from datetime import date

female_names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_names = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']
min_age = 3
max_age = 50


def generate_email_address(name, surname):
    return f"{name.lower()}.{surname.lower()}@example.com"


def generate_random_age():
    return random.randint(min_age, max_age)


def check_if_person_is_an_adult(age):
    return True if age >= 18 else False


def calculate_the_birth_year(age):
    return date.today().year - age


def generate_person_data(female):
    first_name = random.choice(female_names) if female else random.choice(male_names)
    last_name = random.choice(surnames)
    person_age = generate_random_age()
    return {
        "firstname": first_name,
        "lastname": last_name,
        "country": random.choice(countries),
        "email": generate_email_address(first_name, last_name),
        "age": person_age,
        "adult": check_if_person_is_an_adult(person_age),
        "birth_year": calculate_the_birth_year(person_age)
    }


def generate_list_of_people(number_of_people):
    list_of_people = []
    for i in range(number_of_people):
        female = bool(i % 2)
        person_dictionary = generate_person_data(female)
        list_of_people.append(person_dictionary)
    return list_of_people


def generate_and_print_list_of_people_welcome_message(people_list):
    for person in people_list:
        print(
            f"Hi! I'm {person['firstname']} {person['lastname']}. I come from {person['country']} and I was born in {person['birth_year']}.")


if __name__ == '__main__':
    people = generate_list_of_people(10)
    generate_and_print_list_of_people_welcome_message(people)
