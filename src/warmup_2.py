def return_square_random_number(number):
    return number ** 2


def celsius_to_fahrenheit(temperature_celsius):
    return temperature_celsius * 1.8 + 32


def calculate_cuboid_volume(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # Zadanie 1
    print(return_square_random_number(0))
    print(return_square_random_number(16))
    print(return_square_random_number(2.55))

    # Zadanie 2
    print(celsius_to_fahrenheit(20))

    # Zadanie 3
    print(calculate_cuboid_volume(1, 4, 8))
