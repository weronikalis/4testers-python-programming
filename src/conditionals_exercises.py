# Checking the speed
def display_speed_information(speed):
    if speed > 50:
        print("Slow down!")
    else:
        print("Thank you, your speed is below the limit!")


# Checking the weather conditions
def check_the_weather_conditions(temperature, pressure):
    if temperature == 0 and pressure == 1013:
        return True
    else:
        return False


# Calculating the fine amount for the exceeded speed
def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


# Checking the speed and the consequences
def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print("Your speed was:", speed)
    if speed > 100:
        print("You are loosing your drivers license!")
    elif speed > 50:
        print(f"You are charged with speeding fine! Fine amount is {calculate_fine_amount(speed)}!")
    else:
        print("Congratulations, you are driving safely!")

# Checking the grades
def define_the_grades(grade):
    if not isinstance(grade, float) or isinstance(grade, int):
        return "Type the number!"
    elif grade < 2 or grade > 5:
        return "N/A"
    elif grade >= 4.5:
        return "bardzo dobry"
    elif grade >= 4.0:
        return "dobry"
    elif grade >= 3.0:
        return "dostateczny"
    else:
        return "niedostateczny"

def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

print(check_the_weather_conditions(0, 1013))
print(check_the_weather_conditions(1, 1013))
print(check_the_weather_conditions(0, 1014))
print(check_the_weather_conditions(1, 1014))

print_the_value_of_speeding_fine_in_built_up_area(101)
print_the_value_of_speeding_fine_in_built_up_area(100)
print_the_value_of_speeding_fine_in_built_up_area(49)
print_the_value_of_speeding_fine_in_built_up_area(50)
print_the_value_of_speeding_fine_in_built_up_area(72)
print_the_value_of_speeding_fine_in_built_up_area(99)

print(define_the_grades(2.5))
print(define_the_grades(3.1))
print(define_the_grades(4.0))
print(define_the_grades(1.0))
print(define_the_grades(5.0))
print(define_the_grades(5.3))
print(define_the_grades("ocena"))


