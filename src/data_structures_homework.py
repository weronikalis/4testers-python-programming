def describe_player(player_dictionary):
    nick = player_dictionary["nick"]
    type = player_dictionary["type"]
    exp = player_dictionary["exp"]
    print(f"The player {nick} is of type {type} and has {exp} EXP")


if __name__ == '__main__':

    player_1 = {
    "nick": "maestro_54",
    "type": "warrior",
    "exp": 3000
    }

    player_2 = {
    "nick": "miami_22",
    "type": "elf",
    "exp": 1000
    }

describe_player(player_1)
describe_player(player_2)
