movies = ['Before Sunrise', 'Lord of the Rings', 'Inception', 'Grand Budapest Hotel','Baraka']

last_movie = movies[-1]
movies.append('Blade Runner')
movies.append('Titanic')
print(len(movies))
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'Top Gun 2')
print(movies)

emails = ['a@example.com', 'b@example.com']
print(len(emails))
print(emails[0])
print(emails[-1])
emails.insert(2,'cde@example.com')
print(emails)


friend = {
    "name": "Karolina",
    "age": 30,
    "hobby": ["photography", "cooking"]
}

friend_hobbies = friend["hobby"]
print("Hobbies of my friend", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")

friend["hobby"].append("Football")
print(friend)
friend["married"] = False
friend["age"] = 34
print(friend)

