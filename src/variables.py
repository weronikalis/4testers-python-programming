first_name = "Weronika"
last_name = "Lis"
age = 30

print(first_name)
print(last_name)
print(age)

# I am defining a set of variables describing my dog
name = "Chrupek"
coat_color = "red"
age_in_months = 30
weight = 10.1
number_of_paws = 4
is_male = True

print(name, age_in_months, weight, number_of_paws, coat_color, is_male)

#I am defining a set of variables describing my best friend

friend_name = "Karolina"
friend_age = 30
friend_animals_quantity = 1
friend_driving_license = True
friendship_duration = 23.5

print("Friend's name:", friend_name, sep='\t')
print(friend_name, friend_age, friend_animals_quantity, friend_driving_license, friendship_duration, sep=', ')


#my_bank_account_total = 6_000_000
#my_friends_account_total = 5e12

#print(my_bank_account_total)
#print(my_friends_account_total)



